#include <vulkan/vulkan.h>
#include <vulkan/vk_enum_string_helper.h>
#include <cassert>
#include <cstdint>

#include <stdexcept>
#include <source_location>
#include <vector>
#include <iostream>

#include <shader/test.h>

struct Exception
{
    VkResult result;
    std::source_location location;
};

#define attempt(result) do {if(VK_SUCCESS != result) throw Exception{result, std::source_location::current()};} while(0)

using u32 = std::uint32_t;

struct Push
{
    u32 bufferSize;
    float f;
};

static_assert(offsetof(Push, bufferSize) == 0u);
static_assert(offsetof(Push, f) == 4u);
static_assert(sizeof(Push) == 8u);

int main()
{
    try
    {
        VkInstance instance;

        {
            char const * const layers[] =
            {
                "VK_LAYER_KHRONOS_validation",
            };
            char const * const extensions[] =
            {
                "VK_KHR_portability_enumeration",
            };
            VkApplicationInfo const applicationInfo =
            {
                .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
                .pNext = nullptr,
                .pApplicationName = "learnvulkan",
                .applicationVersion = 0u,
                .pEngineName = nullptr,
                .engineVersion = 0u,
                .apiVersion = VK_API_VERSION_1_1
            };
            VkInstanceCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
                .pNext = nullptr,
                .flags = VK_INSTANCE_CREATE_ENUMERATE_PORTABILITY_BIT_KHR,
                .pApplicationInfo = &applicationInfo,
                .enabledLayerCount = std::size(layers),
                .ppEnabledLayerNames = layers,
                .enabledExtensionCount = std::size(extensions),
                .ppEnabledExtensionNames = extensions,
            };
            attempt(vkCreateInstance(&createInfo, nullptr, &instance));
        }

        u32 deviceCount = 0;
        attempt(vkEnumeratePhysicalDevices(instance, &deviceCount, nullptr));

        std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
        attempt(vkEnumeratePhysicalDevices(instance, &deviceCount, physicalDevices.data()));

        VkPhysicalDevice const physicalDevice = physicalDevices[0];
        {
            VkPhysicalDeviceProperties properties;
            VkPhysicalDeviceFeatures   features;
            vkGetPhysicalDeviceProperties(physicalDevice, &properties);
            vkGetPhysicalDeviceFeatures  (physicalDevice, &features  );
        }
        VkPhysicalDeviceMemoryProperties memoryProps;
        vkGetPhysicalDeviceMemoryProperties(physicalDevice, &memoryProps);

        /*
        struct Memory
        {
            VkMemoryType type;
            VkMemoryHeap heap;
        };
        std::vector<Memory> memory(memoryProps.memoryTypeCount);
        for(u32 i = 0u; i < memoryProps.memoryTypeCount; ++i)
        {
            VkMemoryType const type = memoryProps.memoryTypes[i];
            VkMemoryHeap const heap = memoryProps.memoryHeaps[type.heapIndex];
            memory[i] = {type, heap};
        }
        */

        u32 queueCount = 0u;
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, nullptr);
        std::vector<VkQueueFamilyProperties> queueProperties(queueCount);
        vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, &queueCount, queueProperties.data());

        u32 queueFamily = 0u;
        for(; queueFamily < queueCount; ++queueFamily)
            if((VK_QUEUE_COMPUTE_BIT & queueProperties[queueFamily].queueFlags) != 0u)
                break;
        if(queueFamily == queueCount)
            throw std::runtime_error("no compute queue family");

        VkDevice device;
        {
            float const queuePriorities[] = {1.f};
            char const * const extensions[] =
            {
                "VK_KHR_portability_subset",
            };
            VkDeviceQueueCreateInfo const queueCreateInfo =
            {
                .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .queueFamilyIndex = queueFamily,
                .queueCount = 1u,
                .pQueuePriorities = queuePriorities,
            };
            VkDeviceCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .queueCreateInfoCount = 1u,
                .pQueueCreateInfos = &queueCreateInfo,
                .enabledLayerCount = 0u,
                .ppEnabledLayerNames = nullptr,
                .enabledExtensionCount = std::size(extensions),
                .ppEnabledExtensionNames = extensions,
                .pEnabledFeatures = nullptr,
            };
            attempt(vkCreateDevice(physicalDevice, &createInfo, nullptr, &device));
        }

        VkQueue queue;
        vkGetDeviceQueue(device, queueFamily, 0, &queue);

        u32 const bufferSize = 256u * sizeof(float);
        VkBuffer buffer;
        {
            VkBufferCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .size                   = bufferSize,
                .usage                  = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
                .sharingMode            = VK_SHARING_MODE_EXCLUSIVE,
                .queueFamilyIndexCount  = 0u,
                .pQueueFamilyIndices    = nullptr,
            };
            attempt(vkCreateBuffer(device, &createInfo, nullptr, &buffer));
        }
        VkDeviceMemory bufferMemory;
        {
            VkMemoryRequirements requirements;
            vkGetBufferMemoryRequirements(device, buffer, &requirements);

            u32 memoryTypeIndex = 0u;
            for(; memoryTypeIndex < memoryProps.memoryTypeCount; ++memoryTypeIndex)
            {
                bool const suitsRequirements = (requirements.memoryTypeBits & (1u << memoryTypeIndex)) != 0u;
                bool const hostVisible = (memoryProps.memoryTypes[memoryTypeIndex].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) != 0u;
                if(suitsRequirements && hostVisible)
                    break;
            }
            if(memoryTypeIndex == memoryProps.memoryTypeCount)
                throw std::runtime_error("no suitable memory type for the buffer found");

            VkMemoryAllocateInfo const allocateInfo =
            {
                .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
                .pNext = nullptr,
                .allocationSize     = requirements.size,
                .memoryTypeIndex    = memoryTypeIndex,
            };
            attempt(vkAllocateMemory(device, &allocateInfo, nullptr, &bufferMemory));
            attempt(vkBindBufferMemory(device, buffer, bufferMemory, 0u));
        }

        VkDescriptorPool descriptorPool;
        {
            VkDescriptorPoolSize const poolSizes[] =
            {{
                .type = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .descriptorCount = 1u,
            }};
            VkDescriptorPoolCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .maxSets = 1u,
                .poolSizeCount = std::size(poolSizes),
                .pPoolSizes = poolSizes,
            };
            attempt(vkCreateDescriptorPool(device, &createInfo, nullptr, &descriptorPool));
        }
        VkDescriptorSetLayout descriptorSetLayout;
        {
            VkDescriptorSetLayoutBinding const bindings[] =
            {{
                .binding = 0u,
                .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .descriptorCount    = 1u,
                .stageFlags         = VK_SHADER_STAGE_COMPUTE_BIT,
                .pImmutableSamplers = nullptr,
            }};
            VkDescriptorSetLayoutCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .bindingCount   = std::size(bindings),
                .pBindings      = bindings,
            };
            attempt(vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout));
        }
        VkDescriptorSet descriptorSet;
        {
            VkDescriptorSetAllocateInfo const allocateInfo =
            {
                .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
                .pNext = nullptr,
                .descriptorPool     = descriptorPool,
                .descriptorSetCount = 1u,
                .pSetLayouts        = &descriptorSetLayout,
            };
            attempt(vkAllocateDescriptorSets(device, &allocateInfo, &descriptorSet));

            VkDescriptorBufferInfo const bufferInfo =
            {
                .buffer = buffer,
                .offset = 0u,
                .range = VK_WHOLE_SIZE,
            };
            VkWriteDescriptorSet const writeDescriptorSet =
            {
                .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                .pNext = nullptr,
                .dstSet             = descriptorSet,
                .dstBinding         = 0u,
                .dstArrayElement    = 0u,
                .descriptorCount    = 1u,
                .descriptorType     = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                .pImageInfo         = nullptr,
                .pBufferInfo        = &bufferInfo,
                .pTexelBufferView   = nullptr,
            };
            vkUpdateDescriptorSets(device, 1u, &writeDescriptorSet, 0u, nullptr);
        }

        VkPipelineLayout pipelineLayout;
        {
            VkPushConstantRange const pushRange =
            {
                .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
                .offset = 0u,
                .size = sizeof(Push),
            };
            VkPipelineLayoutCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .setLayoutCount         = 1u,
                .pSetLayouts            = &descriptorSetLayout,
                .pushConstantRangeCount = 1u,
                .pPushConstantRanges    = &pushRange,
            };
            attempt(vkCreatePipelineLayout(device, &createInfo, nullptr, &pipelineLayout));
        }

        VkPipeline pipeline;
        {
            VkShaderModule shaderModule;
            VkShaderModuleCreateInfo const shaderModuleCreateInfo =
            {
                .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                .pNext      = nullptr,
                .flags      = {},
                .codeSize   = std::size(testCS) * 4u,
                .pCode      = testCS,
            };
            attempt(vkCreateShaderModule(device, &shaderModuleCreateInfo, nullptr, &shaderModule));

            VkPipelineShaderStageCreateInfo const stage =
            {
                .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                .pNext = nullptr,
                .flags = {},
                .stage = VK_SHADER_STAGE_COMPUTE_BIT,
                .module = shaderModule,
                .pName = "main",
                .pSpecializationInfo = nullptr,
            };
            VkComputePipelineCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
                .pNext              = nullptr,
                .flags              = {},
                .stage              = stage,
                .layout             = pipelineLayout,
                .basePipelineHandle = VK_NULL_HANDLE,
                .basePipelineIndex  = -1,
            };
            attempt(vkCreateComputePipelines(device, VK_NULL_HANDLE, 1, &createInfo, nullptr, &pipeline));

            vkDestroyShaderModule(device, shaderModule, nullptr);
        }

        {
            void *pointer;
            attempt(vkMapMemory(device, bufferMemory, 0u, VK_WHOLE_SIZE, {}, &pointer));
            float * const f = static_cast<float *>(pointer);
            for(u32 i = 0u; i < bufferSize; ++i)
                f[i] = float(i);

            VkMappedMemoryRange const range =
            {
                .sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE,
                .pNext = nullptr,
                .memory = bufferMemory,
                .offset = 0u,
                .size = VK_WHOLE_SIZE,
            };
            attempt(vkFlushMappedMemoryRanges(device, 1u, &range));
            vkUnmapMemory(device, bufferMemory);
        }

        VkCommandPool commandPool;
        {
            VkCommandPoolCreateInfo const createInfo =
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                .pNext = nullptr,
                .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
                .queueFamilyIndex = queueFamily,
            };
            attempt(vkCreateCommandPool(device, &createInfo, nullptr, &commandPool));
        }
        VkCommandBuffer commandBuffer;
        {
            
            VkCommandBufferAllocateInfo const allocateInfo =
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                .pNext = nullptr,
                .commandPool        = commandPool,
                .level              = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                .commandBufferCount = 1,
            };
            attempt(vkAllocateCommandBuffers(device, &allocateInfo, &commandBuffer));
        }

        // record command buffer:
        {
            VkCommandBufferBeginInfo const commandBufferBeginInfo =
            {
                .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                .pNext = nullptr,
                .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
                .pInheritanceInfo = nullptr,
            };
            attempt(vkBeginCommandBuffer(commandBuffer, &commandBufferBeginInfo));

            VkBufferMemoryBarrier bufferBarrier =
            {
                .sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER,
                .pNext = nullptr,
                .srcAccessMask          = VK_ACCESS_HOST_WRITE_BIT,
                .dstAccessMask          = VK_ACCESS_SHADER_READ_BIT,
                .srcQueueFamilyIndex    = VK_QUEUE_FAMILY_IGNORED,
                .dstQueueFamilyIndex    = VK_QUEUE_FAMILY_IGNORED,
                .buffer                 = buffer,
                .offset                 = 0u,
                .size                   = VK_WHOLE_SIZE,
            };
            vkCmdPipelineBarrier
            (
                commandBuffer,
                VK_PIPELINE_STAGE_HOST_BIT,
                VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                {},
                0u, nullptr,
                1u, &bufferBarrier,
                0u, nullptr
            );

            Push const push =
            {
                .bufferSize = bufferSize,
                .f = 1.f,
            };
            VkPipelineBindPoint const bindPoint = VK_PIPELINE_BIND_POINT_COMPUTE;
            vkCmdBindPipeline(commandBuffer, bindPoint, pipeline);
            vkCmdBindDescriptorSets(commandBuffer, bindPoint, pipelineLayout, 0, 1u, &descriptorSet, 0, nullptr);
            vkCmdPushConstants(commandBuffer, pipelineLayout, VK_SHADER_STAGE_COMPUTE_BIT, 0, sizeof(Push), &push);
            vkCmdDispatch(commandBuffer, (bufferSize + 31u) / 32u, 1, 1);

            bufferBarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
            bufferBarrier.dstAccessMask = VK_ACCESS_HOST_READ_BIT;
            vkCmdPipelineBarrier
            (
                commandBuffer,
                VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
                VK_PIPELINE_STAGE_HOST_BIT,
                {},
                0u, nullptr,
                1u, &bufferBarrier,
                0u, nullptr
            );

            attempt(vkEndCommandBuffer(commandBuffer));
        }

        {
            VkSubmitInfo const submitInfo =
            {
                .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
                .pNext = nullptr,
                .waitSemaphoreCount = 0u,
                .pWaitSemaphores = nullptr,
                .pWaitDstStageMask = nullptr,
                .commandBufferCount = 1u,
                .pCommandBuffers = &commandBuffer,
                .signalSemaphoreCount = 0u,
                .pSignalSemaphores = nullptr,
            };
            attempt(vkQueueSubmit(queue, 1u, &submitInfo, VK_NULL_HANDLE));
            vkDeviceWaitIdle(device);
        }

        std::vector<float> output(bufferSize);
        {
            void *pointer;
            attempt(vkMapMemory(device, bufferMemory, 0u, VK_WHOLE_SIZE, {}, &pointer));
            float * const f = static_cast<float *>(pointer);
            for(u32 i = 0u; i < bufferSize; ++i)
                output[i] = f[i];
            vkUnmapMemory(device, bufferMemory);
        }

        for(float const f : output)
            std::cout << f << ' ';
        std::cout << std::endl;

        vkDestroyPipeline(device, pipeline, nullptr);
        vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
        vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
        vkDestroyDescriptorPool(device, descriptorPool, nullptr);
        vkFreeCommandBuffers(device, commandPool, 1u, &commandBuffer);
        vkDestroyCommandPool(device, commandPool, nullptr);
        vkFreeMemory(device, bufferMemory, nullptr);
        vkDestroyBuffer(device, buffer, nullptr);
        vkDestroyDevice(device, nullptr);
        vkDestroyInstance(instance, nullptr);
    }
    catch(std::runtime_error const &e)
    {
        std::cerr << e.what() << std::endl;
    }
    catch(Exception const &exception)
    {
        auto const &[result, location] = exception;
        std::cerr << location.file_name    () << ':'
                  << location.line         () << ' '
                  << string_VkResult(result) << std::endl;
    }
}
